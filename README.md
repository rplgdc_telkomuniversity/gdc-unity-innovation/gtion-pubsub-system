# Gtion PubSub System (GHub)
## Built In
![Build Status](https://badgen.net/badge/Build/Passing/green) ![Version](https://badgen.net/badge/Release/v1.0.0/blue) ![Languages](https://badgen.net/badge/Languages/C-Sharp/blue)
![Engine](https://badgen.net/badge/Engine/Unity%202019.4.29f1/gray)

Depedencies
- no depedencies

## Installation
Please check documentation below
> https://docs.unity3d.com/2019.3/Documentation/Manual/upm-ui-giturl.html

## What is it?
A simple PubSub System to Implement Publish and Subscribe Pattern

## What is Publish Subscribe Pattern?
well, it is kinda `event` but there's a singleton object that handle all of it in selected context. please read for more detail.
> https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern

## When is it useful?
Pubsub is very useful thought in several case in game development, It might save a lot of time when creating a manageable architecture.
> https://blog.mgechev.com/2013/04/24/why-to-use-publishsubscribe-in-javascript/

## v1.0.0!
- PubSub System
- Available in Project Context and Scene Context (+ Object Context)
- Allow Function as Handler (not only method)
- Async Method/Function
- Auto UnSubscribe on Selected `Subscriber` when object is destroyed.

## Usage
#### Getting Started Example
Create an empty class
the pubsub system will group the subscriber according to the class type
```
public class TestC
{ 
}
```

Create a Subscriber
in this example, i using `Gtion Custom Inspector` ([Source](https://gitlab.com/rplgdc_telkomuniversity/gdc-unity-innovation/gtion-custom-inspector)) to help visualize in the inspector
```
using UnityEngine;
using Gtion.Plugin.CustomInspector.Button;
// [Important] Add this package
using Gtion.Plugin.PubSub;

public class TestSub : MonoBehaviour
{
    [Button]
    void Sub()
    {
        GHub.Project.Subscribe<TestC>(Test);
    }

    [Button]
    void UnSub()
    {
        GHub.Project.Unsubscribe<TestC>(Test);
    }

    // Update is called once per frame
    void Test(TestC a)
    {
        Debug.Log("test"+gameObject.name , gameObject);
    }
}
```

Create a Publisher
```
using UnityEngine;
using Gtion.Plugin.PubSub;

public class testPub : MonoBehaviour
{
    void Update()
    {
        GHub.Project.Publish(new TestC());
    }
}
```
#### Context
there's 2 context available in this package which `Project` and `Scene` context.
but not limited, you could also create a `Object` context.

###### Project Context
Project Context is `PubSub for Project Context that always exist when the game is played`

Which is, this pubsub will not deleted in whatever it case, as long as the game is running.
this will be usefull when you're trying to call a specific menu that exist in wherever, such as `Premium Shop`, `Settings`, etc.

How do i use project context?
```
// use like this
GHub.Project
```

###### Scene Context
Scene Context is `PubSub for Scene Context that will exist in current scene context, *it will be deleted when the scene unloaded`

Which is, this pubsub will deleted when the scene is `Unloaded` or `Destroyed`. this package will create a new one when a new scene is loaded.
this will be usefull when you're trying to call a specific menu that exist in current scene only, and you dont need to worry of `memory leak` because unsubscribed event.

How do i use scene context?
```
// use like this
GHub.Scene
```

###### Object Context
Object context is actually a custom context that you created by yourself. this context should be attached to a object and will be deleted together when main object is deleted.

How do i use Object context?
```
using Gtion.Plugin.PubSub;

public class SomeClass
{
    // just use the GHub class
    GHub hub = new GHub();
}
```

#### Usefull API
###### Publish
```
//Publish an Event to All Subscriber
public void Publish<T>(T data = default(T))

//Publish an Event to All Subscriber Asynchronously
public async Task PublishAsync<T>(T data = default(T))
```
###### Subscribe
```
// Subscribe a Type(T), listen to any publisher event
public void Subscribe<T>(Action<T> handler)

// Subscribe a Type(T) tied to subscriber(object), listen to any publisher event
// Note : Event will deleted when subscriber object destroyed
public void Subscribe<T>(object subscriber, Action<T> handler)

// Subscribe a Type(T), listen to any publisher event
public void Subscribe<T>(Func<T, Task> handler)

// Subscribe a Type(T) tied to subscriber(object), listen to any publisher event
// Note : Event will deleted when subscriber object destroyed
public void Subscribe<T>(object subscriber, Func<T, Task> handler)
```
###### UnSubscribe
```
//Unsubscribing all subscriber and their Event.
public void Unsubscribe()

//Unsubscribing all Event of a subscriber(object).
public void Unsubscribe(object subscriber)

//Unsubscribing a Type of Event in all subscriber.
public void Unsubscribe<T>()

//Unsubscribing a specific Event in all subscriber.
public void Unsubscribe<T>(Action<T> handler)

//Unsubscribing a specific Event in a specific subscriber.
public void Unsubscribe<T>(object subscriber, Action<T> handler = null)
```

###### UnSubscribe
```
//Checking if a Type of Event Exist
public bool Exists<T>()

//Checking if a Type of Event Exist in a subscriber
public bool Exists<T>(object subscriber)

//Checking if a specific Event Exist in a subscriber
public bool Exists<T>(object subscriber, Action<T> handler)
```

#### TL;DR
to implement PubSub system you need 3 step
###### 1. First add the library into your script 
```
using Gtion.Plugin.PubSub;
```
###### 2. Add the subscriber 
you can use either `Project` (Project Context) or `Scene` (Scene Context).
```
GHub.Project.Subscribe<TestC>(Test);
```
###### 3. Call Method from a Publisher 
```
GHub.Project.Publish(new TestC());
```

## Contributor


| Profile | 
| ------ |
| [![Firdiar](https://gitlab.com/uploads/-/system/user/avatar/2307294/avatar.png?raw=true)](https://www.linkedin.com/in/firdiar) |
| [Firdiansyah Ramadhan](https://www.linkedin.com/in/firdiar) | 

##### Contributor Notes
```
Hope it help you guys alot! :) -Firdi
```

## License

MIT

**Free Software, Hell Yeah!**

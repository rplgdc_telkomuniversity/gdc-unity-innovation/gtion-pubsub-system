using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gtion.Plugin.PubSub
{
    public class GHub
    {

        #region Static
        private static GHub _project;
        private static GHub _scene;

        /// <summary>
        /// PubSub for Project Context that always exist when the game is played.
        /// </summary>
        public static GHub Project => _project;
        /// <summary>
        /// PubSub for Scene Context that will exist in current scene context, means it will be deleted when the scene changed.
        /// </summary>
        public static GHub Scene => _scene;

        #region Unity Callback
        [RuntimeInitializeOnLoadMethod]
        public static void Initialize()
        {
            if(_project==null)
                _project = new GHub();

            if(_scene==null)
                _scene = new GHub();

            SceneManager.sceneUnloaded += ReInitSceneContext;
        }

        public static void ReInitSceneContext(Scene scene) 
        {
            _scene = new GHub();
        }
        #endregion
        #endregion

        internal List<Handler> _handlers = new List<Handler>();
        internal object _locker = new object();

        #region Public Publish
        /// <summary>
        /// Publish an Event to All Subscriber
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        public void Publish<T>(T data = default(T))
        {
            foreach (var handler in GetAliveHandlers<T>())
            {
                switch (handler.Action)
                {
                    case Action<T> action:
                        action(data);
                        break;
                    case Func<T, Task> func:
                        func(data);
                        break;
                    case Action meth:
                        meth();
                        break;
                }
            }
        }

        /// <summary>
        /// Publish an Event to All Subscriber Asynchronously
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task PublishAsync<T>(T data = default(T))
        {
            foreach (var handler in GetAliveHandlers<T>())
            {
                switch (handler.Action)
                {
                    case Action<T> action:
                        action(data);
                        break;
                    case Func<T, Task> func:
                        await func(data);
                        break;
                    case Action meth:
                        meth();
                        break;
                }
            }
        }
        #endregion

        #region Public Subscribe
        /// <summary>
        /// Subscribe a Type(T), listen to any publisher event
        /// </summary>
        /// <typeparam name="T">Type identifier</typeparam>
        /// <param name="handler">Callback Event</param>
        public void Subscribe<T>(Action handler)
        {
            SubscribeDelegate<T>(this, handler);
        }

        /// <summary>
        /// Subscribe a Type(T), listen to any publisher event
        /// </summary>
        /// <typeparam name="T">Type identifier</typeparam>
        /// <param name="handler">Callback Event</param>
        public void Subscribe<T>(Action<T> handler)
        {
            Subscribe(this, handler);
        }

        /// <summary>
        /// Subscribe a Type(T) tied to subscriber(object), listen to any publisher event
        /// Note : Event will deleted when subscriber object destroyed
        /// </summary>
        /// <typeparam name="T">Type identifier</typeparam>
        /// <param name="subscriber"></param>
        /// <param name="handler">Callback Event</param>
        public void Subscribe<T>(object subscriber, Action<T> handler)
        {
            SubscribeDelegate<T>(subscriber, handler);
        }

        public void Subscribe<T>(Func<T, Task> handler)
        {
            Subscribe(this, handler);
        }

        public void Subscribe<T>(object subscriber, Func<T, Task> handler)
        {
            SubscribeDelegate<T>(subscriber, handler);
        }
        #endregion

        #region Public UnSubscribe
        /// <summary>
        ///     Allow unsubscribing directly to this Hub.
        /// </summary>
        public void Unsubscribe()
        {
            Unsubscribe(this);
        }

        public void Unsubscribe(object subscriber)
        {
            lock (_locker)
            {
                var query = _handlers.Where(a => !a.Sender.IsAlive ||
                                                a.Sender.Target.Equals(subscriber));

                foreach (var h in query.ToList())
                    _handlers.Remove(h);
            }
        }

        /// <summary>
        ///     Allow unsubscribing directly to this Hub.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void Unsubscribe<T>()
        {
            Unsubscribe<T>(this);
        }

        /// <summary>
        ///     Allow unsubscribing directly to this Hub.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler"></param>
        public void Unsubscribe<T>(Action handler)
        {
            Unsubscribe<T>(this, handler);
        }

        /// <summary>
        ///     Allow unsubscribing directly to this Hub.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler"></param>
        public void Unsubscribe<T>(Action<T> handler)
        {
            Unsubscribe(this, handler);
        }

        public void Unsubscribe<T>(object subscriber, Action handler)
        {
            lock (_locker)
            {
                var query = _handlers.Where(a => !a.Sender.IsAlive ||
                                                a.Sender.Target.Equals(subscriber) && a.Type == typeof(T));

                if (handler != null)
                    query = query.Where(a => a.Action.Equals(handler));

                foreach (var h in query.ToList())
                    _handlers.Remove(h);
            }
        }

        public void Unsubscribe<T>(object subscriber, Action<T> handler = null)
        {
            lock (_locker)
            {
                var query = _handlers.Where(a => !a.Sender.IsAlive ||
                                                a.Sender.Target.Equals(subscriber) && a.Type == typeof(T));

                if (handler != null)
                    query = query.Where(a => a.Action.Equals(handler));

                foreach (var h in query.ToList())
                    _handlers.Remove(h);
            }
        }
        #endregion

        #region Public Exist
        public bool Exists<T>()
        {
            return Exists<T>(this);
        }

        public bool Exists<T>(object subscriber)
        {
            lock (_locker)
            {
                foreach (var h in _handlers)
                {
                    if (Equals(h.Sender.Target, subscriber) &&
                         typeof(T) == h.Type)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool Exists<T>(object subscriber, Action<T> handler)
        {
            lock (_locker)
            {
                foreach (var h in _handlers)
                {
                    if (Equals(h.Sender.Target, subscriber) &&
                         typeof(T) == h.Type &&
                         h.Action.Equals(handler))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        #endregion

        #region Core
        private void SubscribeDelegate<T>(object subscriber, Delegate handler)
        {
            var item = new Handler
            {
                Action = handler,
                Sender = new WeakReference(subscriber),
                Type = typeof(T)
            };

            lock (_locker)
            {
                _handlers.Add(item);
            }
        }

        private List<Handler> GetAliveHandlers<T>()
        {
            PruneHandlers();
            return _handlers.Where(h => h.Type.GetTypeInfo().IsAssignableFrom(typeof(T).GetTypeInfo())).ToList();
        }

        private void PruneHandlers()
        {
            lock (_locker)
            {
                for (int i = _handlers.Count - 1; i >= 0; --i)
                {
                    if (!_handlers[i].Sender.IsAlive)
                        _handlers.RemoveAt(i);
                }
            }
        }
        #endregion

        internal class Handler
        {
            public Delegate Action { get; set; }
            public WeakReference Sender { get; set; }
            public Type Type { get; set; }
        }
    }
}
